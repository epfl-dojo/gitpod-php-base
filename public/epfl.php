<?php

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Hello Dojo</title>
    <link rel="stylesheet" href="https://web2018.epfl.ch/5.1.1/css/elements.min.css">
    <script src="https://web2018.epfl.ch/5.1.1/js/elements.min.js"></script>

    <link rel="stylesheet" href="css/main.css" />
    <link rel="icon" href="images/favicon.png" />
  </head>

  <body>
    <div class="site">
      <nav class="access-nav" aria-label="Raccourcis de navigation">
        <ul>
          <li>
            <a class="btn btn-primary" href="/" title="[ALT + 1]" accesskey="1">Page d'accueil du site</a>
          </li>
          <li>
            <a class="btn btn-primary" href="#main" title="[ALT + 2]" accesskey="2">Accéder au contenu</a>
          </li>
          <li>
            <a class="btn btn-primary" href="#main-navigation" title="[ALT + 3]" accesskey="3">Accéder à la navigation principale</a>
          </li>
          <li>
            <a class="btn btn-primary" href="#nav-aside" title="[ALT + 4]" accesskey="4">Accéder à la navigation latérale</a>
          </li>
          <li>
            <a class="btn btn-primary" href="#search" title="[ALT + 5]" accesskey="5">Accéder à la recherche</a>
          </li>
          <li>
            <a class="btn btn-primary" href="mailto:email@example.com" title="[ALT + 6]" accesskey="6">Nous contacter</a>
          </li>
        </ul>
      </nav>

      <header role="banner" class="header">
        <a class="logo" href="#">
          <img src="https://web2018.epfl.ch/5.1.1/icons/epfl-logo.svg" alt="Logo EPFL, École polytechnique fédérale de Lausanne" class="img-fluid">
        </a>
        <ul aria-hidden="true" class="nav-header d-none d-xl-flex">
                <li id="menu-item-1">
                      <a class="nav-item" href="https://www.epfl.ch/about/fr/">À propos</a>
                                    </li>
                <li id="menu-item-2" class="current-menu-item">
                      <a class="nav-item" href="https://www.epfl.ch/education/fr/">Éducation</a>
                                    </li>
                <li id="menu-item-3">
                      <a class="nav-item" href="https://www.epfl.ch/research/fr/">Recherche</a>
                                    </li>
                <li id="menu-item-4">
                      <a class="nav-item" href="https://www.epfl.ch/innovation/fr/">Innovation</a>
                                    </li>
                <li id="menu-item-5">
                      <a class="nav-item" href="https://www.epfl.ch/schools/fr/">Facultés</a>
                                    </li>
                <li id="menu-item-6">
                      <a class="nav-item" href="https://www.epfl.ch/campus/fr/">Campus</a>
                                    </li>
            </ul>
        
      <div class="dropdown dropright search d-none d-xl-block">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
          <svg class="icon" aria-hidden="true"><use xlink:href="#icon-search"></use></svg>
        </a>
        <form action="#" class="dropdown-menu border-0 p-0">
          <div class="search-form mt-1 input-group">
                  <label for="search" class="sr-only">Rechercher sur le site</label>
            <input type="text" class="form-control" name="search" placeholder="Rechercher">
            <button type="submit" class="d-none d-xl-block btn btn-primary input-group-append">Valider</button></div>
                          </form>
      </div>
        
      <form action="#" class="d-xl-none">
        <a id="search-mobile-toggle" class="search-mobile-toggle searchform-controller" href="#">
          <svg class="icon" aria-hidden="true"><use xlink:href="#icon-search"></use></svg>
              <span class="toggle-label sr-only">Afficher / masquer le formulaire de recherche</span>
                </a>
        <div class="input-group search-mobile" role="search">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <svg class="icon" aria-hidden="true"><use xlink:href="#icon-search"></use></svg>
            </span>
          </div>
              <label for="search" class="sr-only">Rechercher sur le site</label>
          <input type="text" class="form-control" name="search" placeholder="Rechercher">
                  <div class="input-group-append">
            <a id="search-mobile-close" class="search-mobile-close searchform-controller" href="#">
              <svg class="icon" aria-hidden="true"><use xlink:href="#icon-close"></use></svg>
                      <span class="toggle-label sr-only">Masquer le formulaire de recherche</span>
                            </a>
          </div>
        </div>
      </form>        
      <nav class="nav-lang nav-lang-short ml-auto">
        <ul>
              <li>
            <span class="active" aria-label="Français">FR</span>
          </li>
          <li>
            <a href="#" aria-label="English">EN</a>
          </li>
                    </ul>
      </nav>
          <div class="btn btn-secondary nav-toggle-mobile d-xl-none">
        <span class="label">Menu</span>
        <div class="hamburger">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      </header>

      <div class="main-container">
              
        <div>

                




      <div class="overlay"></div>
      <nav class="nav-main" id="main-navigation" role="navigation">
        <div class="nav-wrapper">
          <div class="nav-container">
            <ul class="nav-menu">
                                    <li class="
                      current-menu-ancestor
                          menu-item-has-children">
          <a href="https://www.epfl.ch/about/fr/">À propos</a>
                <a href="#" role="button" aria-hidden="true" class="nav-arrow">
              <div class="icon-container">
                <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
              </div>
            </a>
            <ul>
              <li class="nav-back">
                <a href="https://www.epfl.ch/about/fr/">
                  <svg class="icon" aria-hidden="true"><use xlink:href="#icon-arrow-left"></use></svg>
      À propos
                </a>
              </li>
                        <li class="
              ">
          <a href="https://www.epfl.ch/about/presidency/fr/accueil/">Présidence</a>
            </li>

                        <li class="
                      current-menu-parent
                    menu-item-has-children">
          <a href="https://www.epfl.ch/about/overview/fr/presentation/">Présentation</a>
                <a href="#" role="button" aria-hidden="true" class="nav-arrow">
              <div class="icon-container">
                <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
              </div>
            </a>
            <ul>
              <li class="nav-back">
                <a href="https://www.epfl.ch/about/overview/fr/presentation/">
                  <svg class="icon" aria-hidden="true"><use xlink:href="#icon-arrow-left"></use></svg>
      Présentation
                </a>
              </li>
                        <li class="
              ">
          <a href="https://www.epfl.ch/about/overview/fr/histoire/">Histoire de l'EPFL</a>
            </li>

                        <li class="
                current-menu-item
              menu-item-has-children">
          <a href="https://www.epfl.ch/about/overview/fr/statistiques-institutionnelles/">Statistiques Institutionnelles</a>
                <a href="#" role="button" aria-hidden="true" class="nav-arrow">
              <div class="icon-container">
                <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
              </div>
            </a>
            <ul>
              <li class="nav-back">
                <a href="https://www.epfl.ch/about/overview/fr/statistiques-institutionnelles/">
                  <svg class="icon" aria-hidden="true"><use xlink:href="#icon-arrow-left"></use></svg>
      Statistiques Institutionnelles
                </a>
              </li>
                        <li class="
              menu-item-has-children">
          <a href="https://www.epfl.ch/about/overview/fr/statistiques-institutionnelles/statistiques-rankings/">Statistiques Rankings</a>
                <a href="#" role="button" aria-hidden="true" class="nav-arrow">
              <div class="icon-container">
                <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
              </div>
            </a>
            <ul>
              <li class="nav-back">
                <a href="https://www.epfl.ch/about/overview/fr/statistiques-institutionnelles/statistiques-rankings/">
                  <svg class="icon" aria-hidden="true"><use xlink:href="#icon-arrow-left"></use></svg>
      Statistiques Rankings
                </a>
              </li>
                        <li class="
              ">
          <a href="https://www.epfl.ch/about/overview/fr/statistiques-institutionnelles/statistiques-rankings/glossaire-rankings/">Glossaire Rankings</a>
            </li>

                  </ul>
            </li>

                  </ul>
            </li>

                        <li class="
              ">
          <a href="https://www.epfl.ch/about/overview/fr/identite/">Identité</a>
            </li>

                  </ul>
            </li>

                  </ul>
            </li>

                            <li class="
              menu-item-has-children">
          <a href="https://www.epfl.ch/education/fr/education/">Éducation</a>
                <a href="#" role="button" aria-hidden="true" class="nav-arrow">
              <div class="icon-container">
                <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
              </div>
            </a>
            <ul>
              <li class="nav-back">
                <a href="https://www.epfl.ch/education/fr/education/">
                  <svg class="icon" aria-hidden="true"><use xlink:href="#icon-arrow-left"></use></svg>
      Éducation
                </a>
              </li>
                        <li class="
              ">
          <a href="https://www.epfl.ch/education/bachelor/fr/index-fr-html/">Bachelor</a>
            </li>

                        <li class="
              ">
          <a href="https://www.epfl.ch/education/master/fr/index-fr-html/">Master</a>
            </li>

                        <li class="
              ">
          <a href="https://www.epfl.ch/education/phd/fr/index-fr-html/">Doctorat</a>
            </li>

                  </ul>
            </li>

                            <li class="
              ">
          <a href="https://www.epfl.ch/research/fr/recherche/">Recherche</a>
            </li>

                            <li class="
              ">
          <a href="https://www.epfl.ch/innovation/fr/innovation-2/">Innovation</a>
            </li>

                    </ul>
          </div>
        </div>
      </nav>
          
          <div class="w-100 pb-5">

                    <main id="main" role="main" class="content container-grid">
                  <div class="alert alert-info">
          <h4>container-grid</h4>
          <p>The <strong>.container-grid</strong> class is used to wrap the whole content of a page. All the direct childs of this wrapper (text blocks for example) will have a maximum width, to ensure a good readability</p>
        </div>

        <div class="container-full">
          <div class="alert alert-warning">
            <h4>container-full</h4>
            <p>Inside of the <strong>.container-grid</strong>, you can use the <strong>.container-full</strong> class to make a component take the full width page.</p>
          </div>
        </div>

        <div class="container">
          <div class="alert alert-success">
            <h4>container</h4>
            <p>This <strong>.container</strong> class is the bootstrap defaut one. When used, It will fallback to the default bootstrap behavior, that can be divided with rows, columns, etc... (see <a href="https://getbootstrap.com/docs/4.1/layout/grid/">the Bootstrap documentation)</a></p>
          </div>
        </div>
              </main>
            
          </div>

            </div>

          <div class="bg-gray-100 pt-5">
          <div class="container">
                    <footer class="footer" role="contentinfo">

        <div class="footer-group footer-sitemap">
            
        <div class="footer-sitemap-col">
          <button class="footer-title collapse-title collapsed" type="button" data-toggle="collapse" data-target="#collapse-footer-1" aria-expanded="false" aria-controls="collapse-footer-1">
                  <strong>À propos</strong>
                            </button>
          <div class="collapse collapse-item" id="collapse-footer-1">
            <ul class="footer-links">
                        <li>
                              <a href="https://www.epfl.ch/about/overview/fr/">Présentation</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/presidency/fr/">Présidence</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/vice-presidencies/fr/">Vice-Présidences</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/campus/fr/">Campus associés</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/working/fr/">Travailler à l'EPFL</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/recruiting/fr/">Recruter des talents de l'EPFL</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/news-and-media/fr/">News &amp; médias</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/sustainability/fr/">Durabilité</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/equality/fr/">Égalité des chances</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/about/philanthropy/fr/">Philanthropie</a>
                                                    </li>
                        <li>
                              <a href="https://essentialtech.center" target="_blank" rel="noopener">Cooperation</a>
                                                    </li>
                        <li>
                              <a href="https://www.epflalumni.ch/fr/" target="_blank" rel="noopener">Alumni</a>
                                                    </li>
                    </ul>
          </div>
        </div>
        <div class="footer-sitemap-col">
          <button class="footer-title collapse-title collapsed" type="button" data-toggle="collapse" data-target="#collapse-footer-2" aria-expanded="false" aria-controls="collapse-footer-2">
                  <strong>Éducation</strong>
                            </button>
          <div class="collapse collapse-item" id="collapse-footer-2">
            <ul class="footer-links">
                        <li>
                              <a href="https://www.epfl.ch/education/bachelor/fr/">Bachelor</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/master/fr/">Master</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/phd/fr/">Doctorat</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/continuing-education/fr/">Formation continue</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/international/fr/">International</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/admission/fr/">Admission</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/studies/">Gestion des études</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/educational-initiatives/fr/">Initiatives pédagogiques</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/education/education-and-science-outreach/fr/">Promotion de l'éducation et des sciences</a>
                                                    </li>
                    </ul>
          </div>
        </div>
        <div class="footer-sitemap-col">
          <button class="footer-title collapse-title collapsed" type="button" data-toggle="collapse" data-target="#collapse-footer-3" aria-expanded="false" aria-controls="collapse-footer-3">
                  <strong>Recherche</strong>
                            </button>
          <div class="collapse collapse-item" id="collapse-footer-3">
            <ul class="footer-links">
                        <li>
                              <a href="https://www.epfl.ch/research/domains/fr/">Domaines de recherche</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/faculty-members/fr/">Nos Professeurs</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/collaborate/fr/">Collaborer avec nos chercheurs</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/facilities/fr/">Utiliser nos infrastructures</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/access-technology/fr/">Accéder à nos technologies</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/services/fr/">Services pour nos laboratoires</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/ethic-statement/fr/">Déontologie et éthique</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/awards/fr/">Prix et distinctions</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/research/open-science/fr/">Open Science</a>
                                                    </li>
                    </ul>
          </div>
        </div>
        <div class="footer-sitemap-col">
          <button class="footer-title collapse-title collapsed" type="button" data-toggle="collapse" data-target="#collapse-footer-4" aria-expanded="false" aria-controls="collapse-footer-4">
                  <strong>Innovation</strong>
                            </button>
          <div class="collapse collapse-item" id="collapse-footer-4">
            <ul class="footer-links">
                        <li>
                              <a href="https://www.epfl.ch/innovation/domains/fr/">Domaines d'innovation</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/innovation/industry/fr/">Collaboration avec l'industrie</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/innovation/startup/fr/">Unité Start-up</a>
                                                    </li>
                    </ul>
          </div>
        </div>
        <div class="footer-sitemap-col">
          <button class="footer-title collapse-title collapsed" type="button" data-toggle="collapse" data-target="#collapse-footer-5" aria-expanded="false" aria-controls="collapse-footer-5">
                  <strong>Campus</strong>
                            </button>
          <div class="collapse collapse-item" id="collapse-footer-5">
            <ul class="footer-links">
                        <li>
                              <a href="https://www.epfl.ch/campus/visitors/fr/">Visiter l'EPFL</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/services/">Services et ressources</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/library/fr/">Bibliothèque</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/restaurants-shops-hotels/fr/">Restaurants, shops et hôtels</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/events/fr/">Événements</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/sports/">Sport</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/art-culture/fr/">Arts et culture</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/security-safety/">Aide, santé et sécurité</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/spiritual-care/">Aumônerie</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/associations/fr/">Associations</a>
                                                    </li>
                        <li>
                              <a href="https://www.epfl.ch/campus/mobility/fr/">Mobilité et transport</a>
                                                    </li>
                    </ul>
          </div>
        </div>
        </div>

        <div class="footer-group">
          
      <div class="footer-faculties">
        <button class="footer-title collapse-title collapsed" type="button" data-toggle="collapse" data-target="#collapse-fac" aria-expanded="false" aria-controls="collapse-fac">
              Facultés
                    </button>
        <div class="collapse collapse-item" id="collapse-fac">
          <ul class="footer-links">
                <li>
                      <a href="https://www.epfl.ch/schools/enac/fr/">Faculté de l'environnement naturel, architectural et construit <strong>ENAC</strong></a>
                                    </li>
                <li>
                      <a href="https://www.epfl.ch/schools/sb/fr/">Faculté des sciences de base <strong>SB</strong></a>
                                    </li>
                <li>
                      <a href="https://sti.epfl.ch/fr/">Faculté des sciences et techniques de l'ingénieur <strong>STI</strong></a>
                                    </li>
                <li>
                      <a href="https://www.epfl.ch/schools/ic/fr/">Faculté informatique et communications <strong>IC</strong></a>
                                    </li>
                <li>
                      <a href="https://www.epfl.ch/schools/sv/fr/">Faculté des sciences de la vie <strong>SV</strong></a>
                                    </li>
                <li>
                      <a href="https://www.epfl.ch/schools/cdm/fr/">Collège du management de la technologie <strong>CDM</strong></a>
                                    </li>
                <li>
                      <a href="https://www.epfl.ch/schools/cdh/fr/">Collège des humanités <strong>CDH</strong></a>
                                    </li>
                <li>
                      <a href="http://www.epfl.ae" target="_blank" rel="noopener">Middle East <strong>ME</strong></a>
                                    </li>
              </ul>
        </div>
      </div>
        </div>

        <div class="footer-group footer-buttons">
          
      <p class="footer-title footer-title-no-underline">Pratique</p>
      <a href="https://www.epfl.ch/campus/services/" class="btn btn-secondary btn-sm">Services et ressources</a>
      <a href="tel:+41216933000" class="btn btn-secondary btn-sm">Urgences : +41 21 693 3000</a>
      <a href="https://www.epfl.ch/about/overview/contact/" class="btn btn-secondary btn-sm">Contact</a>
      <a href="https://map.epfl.ch/?lang=fr" class="btn btn-secondary btn-sm">Plan</a>


        </div>

        <div class="footer-group footer-socials">
          <p class="footer-title footer-title-no-underline">Suivez l'EPFL sur les réseaux sociaux</p>
      <div class="footer-social">
        
      <a href="https://www.facebook.com/epflcampus" class="social-icon social-icon-facebook social-icon-negative" target="_blank" rel="nofollow noopener">
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-facebook"></use></svg>
        <span class="sr-only">Follow us on Facebook.</span>
      </a>
      <a href="https://twitter.com/epfl_en" class="social-icon social-icon-twitter social-icon-negative" target="_blank" rel="nofollow noopener">
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-twitter"></use></svg>
        <span class="sr-only">Follow us on Twitter.</span>
      </a>
      <a href="http://instagram.com/epflcampus" class="social-icon social-icon-instagram social-icon-negative" target="_blank" rel="nofollow noopener">
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-instagram"></use></svg>
        <span class="sr-only">Follow us on Instagram.</span>
      </a>
      <a href="https://www.youtube.com/user/epflnews" class="social-icon social-icon-youtube social-icon-negative" target="_blank" rel="nofollow noopener">
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-youtube"></use></svg>
        <span class="sr-only">Follow us on Youtube.</span>
      </a>
      <a href="https://www.linkedin.com/school/epfl/" class="social-icon social-icon-linkedin social-icon-negative" target="_blank" rel="nofollow noopener">
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-linkedin"></use></svg>
        <span class="sr-only">Follow us on LinkedIn.</span>
      </a>
      </div>
        </div>

        
      <div class="footer-legal">
        <div class="footer-legal-links">
          <a href="https://www.epfl.ch/about/overview/fr/reglements-et-directives/mentions-legales/">Accessibilité</a>
          <a href="https://www.epfl.ch/about/overview/fr/reglements-et-directives/mentions-legales/">Mentions légales</a>
          <a href="https://go.epfl.ch/protection-des-donnees/">Protection des données</a>
        </div>
        <div>
          <p>© 2021 EPFL, tous droits réservés</p>
        </div>
      </div>

      </footer>
        
      <button id="back-to-top" class="btn btn-primary btn-back-to-top">
        <span class="sr-only">Back to top</span>
        <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-top"></use></svg>
      </button>

    <!-- life's too short for these div -->
    </div>
    </div>
    </div>

    </div>
    <script src="js/scripts.js"></script>
  </body>
</html>
