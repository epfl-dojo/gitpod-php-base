<?php

// This is a PHP file.

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Hello Dojo</title>
    <link rel="stylesheet" href="https://web2018.epfl.ch/5.1.1/css/elements.min.css">
    <script src="https://web2018.epfl.ch/5.1.1/js/elements.min.js"></script>

    <link rel="stylesheet" href="css/main.css" />
    <link rel="icon" href="images/favicon.png" />
  </head>

  <body>
  <?php 
    include("./action.php");
  ?>
  <form action="index.php" method="post">
    <p>Nombre : <input type="number" name="nombre" /></p>
    <p><input type="submit" value="OK"></p>
    <input type="hidden" name="target" value="4" />
  </form>
<?php

    echo '<h1>Hello Rocco !</h1>';

?>

    <ul>
        <li><a href="./info.php">phpinfo();</a></li>
        <li><a href="./epfl.php">EPFL Layout</a></li>
    </ul>

    <script src="js/scripts.js"></script>
  </body>
</html>
