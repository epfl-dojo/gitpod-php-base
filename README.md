# gipod-php-base

A demo/base php project to use with gitpod.

## Getting started

Open this project on gitpod, run `php -S 0.0.0.0:3000 -t public/`
and open the browser or the preview from the remote explorer.
